# Changelog

All notable changes are documented in this file using the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## [1.1.1-beta] - 2018-07-15

### Added

* Removed unnecessary development file for the final package.

## [1.1.0-beta] - 2018-07-15

### Added

* First version finalize. AbstractView, AbstractRenderableView, interface for TemplateEngine. Tests 100% coverage.
