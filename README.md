# Sophie View

Provides basic support to represent Views.
Normally we need a View when part of the DOM needs to change
dynamically and respond to some events. Extending your Views from
this abstract class will give your Views an structured
way of defining this common behavior.

**Part of the framework [Sophie](https://bitbucket.org/xtrimsystems/sophie)**

[![npm version](https://badge.fury.io/js/sophie-view.svg)](https://badge.fury.io/js/sophie-view)
[![npm dependencies](https://david-dm.org/xtrimsystems/sophie-view.svg)](https://www.npmjs.com/package/sophie-view?activeTab=dependencies)
[![npm downloads](https://img.shields.io/npm/dm/sophie-view.svg)](https://www.npmjs.com/package/sophie-view)

## INSTALLATION
```shell
yarn add sophie-view
```

## USAGE

### AbstractView

Simple class to represent a DOM element.
When defining your View class you will need to pass the DOM element that represents your View
and define the events that you want to listen from it. When instantiating the View automatically
this one will attach the events.

Having this DOM example
```html
<body>
    <div id="view">
        <div class="foo"></div>
    </div>
</body>
```
>index.ts
```typescript
import { AbstractView, DomEvent } from "sophie-view";

class MyView extends AbstractView
{
	protected events: DomEvent[] = [
		{ name: 'click', queries: ['#view'], callback: this.onEventCallback },
		{ name: 'click', queries: ['.foo'], callback: this.onEventCallback2 },
	];

	public constructor (el: HTMLElement)
	{
		super(el);
	}

	private onEventCallback (e: MouseEvent): void
	{
		console.log('Click on #view element', e);
	}

	private onEventCallback2 (e: MouseEvent): void
	{
		console.log('Click on .foo element', e);
	}
}

const domElement = document.getElementById('view');

const myView = new MyView(domElement);
// Now:
// a click on the div with id "view" will log 'Click on #view element'
// and a click on the div with class "foo" will log 'Click on .foo element'

setTimeout(() => myView.detachEvents(), 5000);
// After 5 seconds all event listeners are removed
```

### AbstractRenderableView

Simple class that extends the AbstractView and that has the ability to
render html dynamically with data. In order to render, you will need to
provide an object that extends the interface TemplateEngine.
You can use the [sophie-mustache-template-engine](https://www.npmjs.com/package/sophie-mustache-template-engine)
module that wraps mustache and extends it to make it compatible with this module.

Having this DOM example
```html
<body>
    <div id="view"></div>
    <div id="view2"></div>
</body>
```
>index.ts
```typescript
import { MustacheHttp, MustacheString } from "sophie-mustache-template-engine";
import { AbstractRenderableView, DomEvent, TemplateEngine } from "sophie-view";

export class MyRenderableView extends AbstractRenderableView
{
	protected events: DomEvent[] = [];

	public constructor (el: HTMLElement, templateEngine: TemplateEngine)
	{
		super(el, templateEngine);
	}
}

const domElementView1 = document.getElementById('view');
const domElementView2 = document.getElementById('view');

// Using from the module sophie-mustache-template-engine the class
// MustacheString we can render a string with HTML into our view
const viewRenderableWithString = new MyRenderableView(
	domElementView1,
	new MustacheString('<div>Hello {{name}}!!</div>'),
);

// Or with MustacheHttp we can simply download a template from the path given
const viewRenderableWithHTMLTemplate = new MyRenderableView(
	domElementView2,
	new MustacheHttp('./template.html'),
);

// The method render returns a Promise so you can control when the
// template has been render

viewRenderableWithString.render({ name: 'world'})
	.then(() => console.log('Render done'))
	.catch((error) => console.log(error));

viewRenderableWithHTMLTemplate.render({ name: 'world'})
	.then(() => console.log('Render done'))
	.catch((error) => console.log(error));

```

## Changelog
[Changelog](https://bitbucket.org/xtrimsystems/sophie-view/src/master/CHANGELOG.md)

## Contributing

[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

## License
This software is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT). See [LICENSE](https://bitbucket.org/xtrimsystems/sophie-view/src/master/LICENSE) for the full license.
