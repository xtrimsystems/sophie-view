export interface DomEvent
{
	name: string;
	queries: string[];
	/*tslint:disable no-any*/
	callback (...args: any[]): any;
}
