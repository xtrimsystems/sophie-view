export interface TemplateEngine
{
	render (element: HTMLElement, data?: object): Promise<HTMLElement>;
}
