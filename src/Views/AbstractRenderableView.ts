import { TemplateEngine } from '../Interfaces';
import { AbstractView } from './AbstractView';

export abstract class AbstractRenderableView extends AbstractView
{
	protected templateEngine: TemplateEngine;

	protected constructor (el: HTMLElement, templateEngine: TemplateEngine)
	{
		super(el);
		this.templateEngine = templateEngine;
	}

	public async render (data?: object): Promise<HTMLElement>
	{
		this.detachEvents();

		return new Promise<HTMLElement>(((resolve, reject) => {
			this.templateEngine.render(this.el, data)
				.then(() => {
					this.attachEvents();
					resolve(this.el);
				})
				.catch(reject);
		}));
	}
}
