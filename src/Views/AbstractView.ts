import { DomEvent } from '../Interfaces';

export const EVENT_LISTENER_ADD = 'addEventListener';
export const EVENT_LISTENER_REMOVE = 'removeEventListener';

export abstract class AbstractView
{
	public el: HTMLElement;
	protected abstract events: DomEvent[];

	protected constructor (el: HTMLElement)
	{
		this.el = el;
		setTimeout(() => this.attachEvents(), 0);
	}

	protected attachEvents (): void
	{
		this.loopThroughEvents(EVENT_LISTENER_ADD);
	}

	protected detachEvents (): void
	{
		this.loopThroughEvents(EVENT_LISTENER_REMOVE);
	}

	private loopThroughEvents (state: string): void
	{
		for (const event of this.events)
		{
			this.loopThroughQueries(event, state);
		}
	}

	private loopThroughQueries (event: DomEvent, state: string): void
	{
		for (const query of event.queries) {
			const querySliced = query.slice(1);
			if (this.el.id === querySliced || this.el.classList.contains(querySliced)) {
				this.toggleEventOnViewItself(event, state);
				continue;
			}

			const elements: NodeListOf<Element> = this.el.querySelectorAll(query);
			this.toggleEventOnElements(event, state, elements);
		}
	}

	private toggleEventOnViewItself (event: DomEvent, state: string)
	{
		switch (state) {
			case EVENT_LISTENER_ADD:
				this.el.addEventListener(event.name, event.callback);
			break;
			case EVENT_LISTENER_REMOVE:
				this.el.removeEventListener(event.name, event.callback);
		}
	}

	private toggleEventOnElements (event: DomEvent, state: string, elements: NodeListOf<Element>)
	{
		const elementsLength = elements.length;
		for (let i = 0; i < elementsLength; i++) {
			switch (state) {
				case EVENT_LISTENER_ADD:
					elements[i].addEventListener(event.name, event.callback);
				break;
				case EVENT_LISTENER_REMOVE:
					elements[i].removeEventListener(event.name, event.callback);
			}
		}
	}
}
