import { DomEvent, TemplateEngine } from "../src/Interfaces";
import { AbstractRenderableView } from "../src/Views";

describe('test AbstractRenderableView', () => {

	describe('method render', () => {
		it('should return a Promise that on resolve return the dom element', () => {

			const mockTemplateEngine = new MockTemplateEngineResolve();
			const testView = new TestView(testElement, mockTemplateEngine);

			testView.render()
				.then((el) => {
					expect(el).toBe(testElement);
				});
		});

		it('should return a Promise that on reject returns an error', () => {

			const mockTemplateEngine = new MockTemplateEngineReject();
			const testView = new TestView(testElement, mockTemplateEngine);

			testView.render()
				.catch((error) => {
					expect(error).toBe(testError);
				});

		});
	});
});

const testElement = document.createElement('DIV');
const testError = new Error('Error on render');

class TestView extends AbstractRenderableView
{
	protected events: DomEvent[] = [];

	public constructor (el: HTMLElement, templateEngine: TemplateEngine)
	{
		super(el, templateEngine);
	}
}

class MockTemplateEngineResolve implements TemplateEngine
{
	public render (element: HTMLElement, data?: object): Promise<HTMLElement> {
		return Promise.resolve(element);
	}
}

class MockTemplateEngineReject implements TemplateEngine
{
	public render (element: HTMLElement, data?: object): Promise<HTMLElement> {
		return Promise.reject(testError);
	}
}
