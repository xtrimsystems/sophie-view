import { AbstractView, DomEvent, EVENT_LISTENER_ADD, EVENT_LISTENER_REMOVE } from '../src';

describe('test AbstractView', () => {

	const el: HTMLElement = document.createElement('DIV');
	const subEl: HTMLElement = document.createElement('DIV');
	let testView: TestView;

	jest.useFakeTimers();

	describe('When create a new instance', () => {

		it('should have the el element initialize and call attachEvents', () => {

			const attachEvents = (TestView as any).prototype.attachEvents;

			const mockAttachEvents = jest.fn();

			(TestView as any).prototype.attachEvents = mockAttachEvents;
			testView = new TestView(el);

			jest.runAllTimers();

			expect(testView.el).toBe(el);
			expect(mockAttachEvents).toHaveBeenCalled();

			(TestView as any).prototype.attachEvents = attachEvents;

			jest.clearAllTimers();
			jest.clearAllMocks();
		});
	});

	describe(('test protected methods'), () => {

		let loopThroughEvents: any;
		let mockLoopThroughEvents: any;

		beforeEach(() => {
			loopThroughEvents = (TestView as any).prototype.loopThroughEvents;
			mockLoopThroughEvents = jest.fn();

			(TestView as any).prototype.loopThroughEvents = mockLoopThroughEvents;
			testView = new TestView(el);
		});

		afterEach(() => {
			(TestView as any).prototype.loopThroughEvents = loopThroughEvents;
			jest.clearAllTimers();
			jest.clearAllMocks();
		});

		describe('method "attachEvents"', () => {

			it('should call method "loopThroughEvents" with expected parameters', () => {

				jest.runAllTimers();
				expect(mockLoopThroughEvents).toHaveBeenCalledTimes(1);
				expect(mockLoopThroughEvents).toHaveBeenCalledWith(EVENT_LISTENER_ADD);

			});

		});

		describe('method "detachEvents"', () => {

			it('should call method "loopThroughEvents" with expected parameters', () => {

				testView.callDetachEvents();
				expect(mockLoopThroughEvents).toHaveBeenCalledTimes(1);
				expect(mockLoopThroughEvents).toHaveBeenCalledWith(EVENT_LISTENER_REMOVE);

			});

		});

	});

	describe('test private methods', () => {

		describe('method "loopThroughEvents"', () => {

			let mockLoopThroughQueries: any;
			let loopThroughQueries: any;

			beforeEach(() => {

				loopThroughQueries = (TestView as any).prototype.loopThroughQueries;
				mockLoopThroughQueries = jest.fn();

				(TestView as any).prototype.loopThroughQueries = mockLoopThroughQueries;
				testView = new TestView(el);
			});

			afterEach(() => {
				(TestView as any).prototype.loopThroughQueries = loopThroughQueries;
				jest.clearAllTimers();
				jest.clearAllMocks();
			});

			it('should loop through the events and call "loopThroughQueries" with expected params', () => {

				jest.runAllTimers();

				expect(mockLoopThroughQueries).toHaveBeenCalledTimes(1);
				expect(mockLoopThroughQueries).toHaveBeenCalledWith((testView as any).events[0], EVENT_LISTENER_ADD);

				testView.callDetachEvents();
				expect(mockLoopThroughQueries).toHaveBeenCalledTimes(2);
				expect(mockLoopThroughQueries).toHaveBeenCalledWith((testView as any).events[0], EVENT_LISTENER_REMOVE);

			});
		});

		describe('method "loopThroughQueries"', () => {

			let toggleEventOnViewItself: any;
			let toggleEventOnElements: any;
			let mockToggleEventOnViewItself: any;
			let mockToggleEventOnElements: any;

			beforeAll(() => {

				toggleEventOnViewItself = (TestView as any).prototype.toggleEventOnViewItself;
				toggleEventOnElements = (TestView as any).prototype.toggleEventOnElements;
				mockToggleEventOnViewItself = jest.fn();
				mockToggleEventOnElements = jest.fn();

				(TestView as any).prototype.toggleEventOnViewItself = mockToggleEventOnViewItself;
				(TestView as any).prototype.toggleEventOnElements = mockToggleEventOnElements;

			});

			afterAll(() => {
				(TestView as any).prototype.toggleEventOnViewItself = toggleEventOnViewItself;
				(TestView as any).prototype.toggleEventOnElements = toggleEventOnElements;
				jest.clearAllTimers();
				jest.clearAllMocks();
			});

			it('should call method "toggleEventOnViewItself" if the query is in the root of the element', () => {

				el.id = 'foo';
				subEl.classList.add('bar');
				el.appendChild(subEl);

				testView = new TestView(el);

				jest.runAllTimers();

				expect(mockToggleEventOnViewItself).toHaveBeenCalledTimes(1);
				expect(mockToggleEventOnViewItself).toHaveBeenCalledWith((testView as any).events[0], EVENT_LISTENER_ADD);
				expect(mockToggleEventOnElements).toHaveBeenCalledTimes(1);
				expect(mockToggleEventOnElements).toHaveBeenCalledWith((testView as any).events[0], EVENT_LISTENER_ADD, el.querySelectorAll('.bar'));

				testView.callDetachEvents();

				expect(mockToggleEventOnViewItself).toHaveBeenCalledTimes(2);
				expect(mockToggleEventOnViewItself).toHaveBeenCalledWith((testView as any).events[0], EVENT_LISTENER_REMOVE);
				expect(mockToggleEventOnElements).toHaveBeenCalledTimes(2);
				expect(mockToggleEventOnElements).toHaveBeenCalledWith((testView as any).events[0], EVENT_LISTENER_REMOVE, el.querySelectorAll('.bar'));

			});
		});

		describe('method "toggleEventOnViewItself"', () => {
			let addEventListener: any;
			let removeEventListener: any;
			let mockAddEventListener: any;
			let mockRemoveEventListener: any;

			beforeAll(() => {

				el.id = 'foo';

				addEventListener = el.addEventListener;
				removeEventListener = el.removeEventListener;
				mockAddEventListener = jest.fn();
				mockRemoveEventListener = jest.fn();

				el.addEventListener = mockAddEventListener;
				el.removeEventListener = mockRemoveEventListener;

			});

			afterAll(() => {
				el.addEventListener = addEventListener;
				el.removeEventListener = removeEventListener;
				jest.clearAllTimers();
				jest.clearAllMocks();
			});

			it('should call addEventListener on the view itself and for the subviews', () => {

				testView = new TestView(el);

				jest.runAllTimers();

				expect(mockAddEventListener).toHaveBeenCalledTimes(1);
				expect(mockAddEventListener).toHaveBeenCalledWith((testView as any).events[0].name, (testView as any).events[0].callback);

				testView.callDetachEvents();

				expect(mockRemoveEventListener).toHaveBeenCalledTimes(1);
				expect(mockRemoveEventListener).toHaveBeenCalledWith((testView as any).events[0].name, (testView as any).events[0].callback);

			});

		});
	});
});

class TestView extends AbstractView
{
	protected events: DomEvent[] = [
		{ name: 'click', queries: ['#foo', '.bar'], callback: () => this.onCallBack },
	];

	public el: HTMLElement;

	public constructor (el: HTMLElement)
	{
		super(el);
	}

	public callDetachEvents (): void
	{
		this.detachEvents();
	}

	public onCallBack (): boolean
	{
		return true;
	}
}
